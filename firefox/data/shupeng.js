var aside = document.getElementById('content').getElementsByClassName('aside')[0],
	title = document.querySelector('h1').querySelector('span'),
	book_name = encodeURIComponent(title.innerHTML),
	_isbn = document.getElementById('info').querySelectorAll('.pl'),
	last_isbn = _isbn[_isbn.length -1];

var next = function(node){
	var ret = node.nextSibling;
	if (ret.nodeType != 1) {
		return ret;
	}else {
		return next(ret);
	}
}

var trim = function(str){
	var str = str.replace(/^\s\s*/, ''),
		ws = /\s/,
		i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i+1);
}

var addlink = function(conf){
	var name = encodeURIComponent(conf.name) || '',
		target = conf.target || false,
		isBlock = conf.isBlock || false,
		style = conf.style || '',
		bid = conf.bid || '';

	var link = document.createElement('a');
	link.href="http://www.shupeng.com/book/"+bid+"?fr=douban2shupeng";
	link.className = 'shupeng-link';
	link.setAttribute('target', '_blank');
	link.setAttribute('style', style);
	link.innerHTML='书朋下载';
	target.appendChild(link);
}

self.postMessage({
	type: 'book_name',
	content: book_name
});

self.port.on('alert', function(data){
	if (data.type == 'search_res') {
		var res = data.content.result.matches;
		var ret = '<div class="gray_ad" id="shupeng-search"><h2><a style="color:#060; background:transparent;" href="http://www.shupeng.com/" target="_blank"><img src="http://www.shupeng.com/favicon.ico" style="vertical-align: text-top;" /> 书朋电子书下载</a></h2>',
			get_book_list = function(o){
				var str = '';
				for (var i = 0; i < o.length; i++) {
					var b = o[i];
					str += '<li style="margin-bottom:5px;padding-bottom:5px; border-bottom:1px solid #"><a style="display:inline-block;" href="http://www.shupeng.com/book/'+b.id+'">'+b.name+'</a> <span style="display:inline-block;">(作者: <a href="http://www.shupeng.com/search/'+encodeURIComponent(b.author)+'" target="_blank">'+b.author+'</a>)</span>';
				};
				return str;
			};
		ret += '<p>电子书第一门户，提供超过800万册电子书免费下载</p><ul class="sps-list">';
		ret += get_book_list(res);
		ret += '<p style="margin:0; text-align:right;"><a href="http://www.shupeng.com/search/'+book_name+'" target="_blank">更多...</a></p></ul></div>';
		aside.innerHTML = ret + aside.innerHTML;
	} else if (data.type == 'download') {
		var o = data.content,
			bid = o.result[isbn];
		if (o.result[isbn]) {
			addlink({
				target: title,
				bid: bid,
				isBlock: false,
				style: 'font-size:50%;margin-left:10px;'
			});
		}
	}
});


if (last_isbn.innerHTML == 'ISBN:') {
	var isbn = trim(next(last_isbn).textContent);

	self.postMessage({
		type: 'isbn',
		content: isbn
	});
}

/*set link.style*/
var style = document.createElement('STYLE');
style.type = 'text/css';
style.textContent = '.shupeng-link {display:inline-block;color:#fff!important;padding:5px 5px 4px;border-radius:5px;background-color:#060; box-shadow:1px 1px 3px #333;} .shupeng-link:hover{color:green!important; background:#fff; box-shadow:1px 1px 3px #ccc;}';
document.querySelector('head').appendChild(style);
