//set appkey from open.shupeng.com
var APPKEY = '68aa965b87f55112baab20a762789fe1';

var data = require("self").data;
var windows = require("windows").browserWindows;
var tabs = require("tabs");
var widgets = require("widget");
var Request = require("request").Request;
var pageMod = require('page-mod');


var widget = widgets.Widget({
    id: "shupeng-link",
    label: "shupeng search",
    contentURL: "http://www.shupeng.com/favicon.ico",
    onClick: function() {
    }
});


pageMod.PageMod({
    include: ['http://book.douban.com/subject/*'],
    contentScriptWhen: 'ready',
    contentScriptFile: [
		data.url("jquery-1.7.1.min.js"),
		data.url("shupeng.js")
	],
    onAttach:function onAttach(worker){
		//get_book_name from document
        worker.on('message', function(data){
			if (data.type == 'book_name') {
				var book_name = data.content;
				//get_search_list from shupeng.com
				Request({
					url: 'http://api.shupeng.com/search?token='+ APPKEY +'&q='+book_name+'&p=1&psize=5',
					onComplete: function(data) {
						worker.port.emit('alert', {
							type:'search_res',
							content: data.json
						});
					}
				}).get();
			}
        });

		//get_isbn from document
        worker.on('message', function(data){
			if (data.type == 'isbn') {
				var isbn = data.content;
				//get_download from shupeng.com
				Request({
					url: 'http://api.shupeng.com/bookid?token='+ APPKEY +'&isbn='+isbn,
					onComplete: function(data) {
						worker.port.emit('alert', {
							type:'download',
							content: data.json
						});
					}
				}).get();
			}
        });
    }
})


console.log("The add-on is running.");
