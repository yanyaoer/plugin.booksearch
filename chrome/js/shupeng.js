var APPKEY = '68aa965b87f55112baab20a762789fe1';

var ajax = function(isbn, fn){
	var req = new XMLHttpRequest();
	req.open('GET', 'http://api.shupeng.com/book?dbg=1&token=68aa965b87f55112baab20a762789fe1&isbn='+isbn, true);
	req.send();
	req.onreadystatechange = function(){
		if (req.readyState == 4 && req.status == 200) fn(req.responseText);
	}
};

var next = function(node){
	var ret = node.nextSibling;
	if (ret.nodeType != 1) {
		return ret;
	}else {
		return next(ret);
	}
}

var trim = function(str){
	var str = str.replace(/^\s\s*/, ''),
		ws = /\s/,
		i = str.length;
	while (ws.test(str.charAt(--i)));
	return str.slice(0, i+1);
}

var addlink = function(conf){
	var name = encodeURIComponent(conf.name) || '',
		target = conf.target || false,
		isBlock = conf.isBlock || false,
		style = conf.style || '',
		bid = conf.bid || '';

	var link = document.createElement('a');
	link.href="http://www.shupeng.com/book/"+bid+"?fr=douban2shupeng";
	link.className = 'shupeng-link';
	link.setAttribute('target', '_blank');
	link.setAttribute('style', style);
	link.innerHTML='书朋下载';
	target.appendChild(link);
}

var path = window.location.pathname,
	aside = document.getElementById('content').getElementsByClassName('aside')[0],
	title = document.querySelector('h1').querySelector('span'),
	book_name = encodeURIComponent(title.innerHTML),
	_isbn = document.getElementById('info').querySelectorAll('.pl'),
	last_isbn = _isbn[_isbn.length -1];


//book-detail
if (path.indexOf('subject') != -1){
	//search-bar
	proxyXHR({
		method: 'GET',
		url: 'http://api.shupeng.com/search?token='+ APPKEY +'&q='+book_name+'&p=1&psize=5',
		onComplete: function(status, data) {
			if (status!=200) return;
			var o = JSON.parse(data),
				res = o.result.matches,
				ret = '<div class="gray_ad" id="shupeng-search"><h2><a style="color:#060; background:transparent;" href="http://www.shupeng.com/" target="_blank"><img src="http://www.shupeng.com/favicon.ico" style="vertical-align: text-top;" />书朋电子书下载</a></h2>',
				get_book_list = function(o){
					var str = '';
					for (var i = 0; i < o.length; i++) {
						var b = o[i];
						str += '<li style="margin-bottom:5px;padding-bottom:5px; border-bottom:1px solid #"><a style="display:inline-block;" href="http://www.shupeng.com/book/'+b.id+'">'+b.name+'</a> <span style="display:inline-block;">(作者: <a href="http://www.shupeng.com/search/'+encodeURIComponent(b.author)+'" target="_blank">'+b.author+'</a>)</span>';
					};
					return str;
				};

			if (res) {
				ret += '<p>电子书第一门户，提供超过800万册电子书免费下载</p><ul class="sps-list">';
				ret += get_book_list(res);
			}else {
				proxyXHR({
					method: 'GET',
					url: 'http://api.shupeng.com/hotbook?token='+ APPKEY +'&p=1&psize=5',
					onComplete: function(status, data) {
						if (status!=200) return;
						var o = JSON.parse(data),
							res = o.result.matches;
						if (res) {
							ret += '<p>暂无'+encodeURIComponent(title.innerHTML)+'相关下载，你可能喜欢...</p><ul class="sps-list">';
							ret += get_book_list(res);
						}
					}
				});
			}
			ret += '<p style="margin:0; text-align:right;"><a href="http://www.shupeng.com/search/'+book_name+'" target="_blank">更多...</a></p></ul></div>';
			aside.innerHTML = ret + aside.innerHTML;
		}
	})

	if (last_isbn.innerHTML == 'ISBN:') {
		var isbn = trim(next(last_isbn).textContent);
		proxyXHR({
			method: 'GET',
			url: 'http://api.shupeng.com/bookid?token='+ APPKEY +'&isbn='+isbn,
			onComplete: function(status, data) {
				if (status!=200) return;
				var o = JSON.parse(data),
					bid = o.result[isbn];
				if (o.result[isbn]) {
					addlink({
						target: title,
						bid: bid,
						isBlock: false,
						style: 'font-size:50%;margin-left:10px;'
					});
				}
			}
		});
	}
}


/*set link.style*/
var style = document.createElement('STYLE');
style.type = 'text/css';
style.textContent = '.shupeng-link {display:inline-block;color:#fff!important;padding:5px 5px 4px;border-radius:5px;background-color:#060; box-shadow:1px 1px 3px #333;} .shupeng-link:hover{color:green!important; background:#fff; box-shadow:1px 1px 3px #ccc;}';
document.querySelector('head').appendChild(style);
